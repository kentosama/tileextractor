#include "mainwindow.h"

#define TILESET_WIDTH 128
#define TILESET_HEIGHT 240

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{

    setWindowTitle("Mega Tileset");
    setMaximumWidth(240);
    setMinimumWidth(240);

    QVBoxLayout *layout = new QVBoxLayout;
    QFrame *frame = new QFrame;
    frame->setLayout(layout);

    button1.setText("Load Image");
    button2.setText("Save tileset");
    button3.setText("About");
    button4.setText("Exit");

    layout->addWidget(&button1);
    layout->addWidget(&button2);
    layout->addWidget(&button3);
    layout->addWidget(&button4);

    connect(&button1, SIGNAL(clicked()), this, SLOT(load()));
    connect(&button2, SIGNAL(clicked()), this, SLOT(save()));
    connect(&button3, SIGNAL(clicked()), this, SLOT(about()));
    connect(&button4, SIGNAL(clicked()), this, SLOT(close()));

    setCentralWidget(frame);

}

MainWindow::~MainWindow()
{

}

void MainWindow::load(void)
{

    QString filename = QFileDialog::getOpenFileName(this, "Open image file", QDir::homePath(), tr("Image (*.png)"), nullptr);
    if(!filename.isEmpty())
    {
       image.load(filename);

       imageTileset = createTileSet();

       preview();

    }
}

void MainWindow::save(void)
{
    QString filename = QFileDialog::getSaveFileName(this, "Save image file", QDir::homePath(), tr("Image (*.png)"), nullptr);

    if(!filename.isEmpty())
    {
        imageTileset.save(filename);
    }
}

void MainWindow::preview(void)
{

    QLabel *label = new QLabel();
    label->setWindowTitle("Tileset preview");

    label->setPixmap(QPixmap::fromImage(imageTileset));
    label->show();
}

void MainWindow::about(void)
{
    QDialog *dialog = new QDialog(this);

    dialog->setWindowTitle("About Mega TileSet");
    dialog->show();
}

QImage MainWindow::createTileSet(void)
{
    extractTileSet();

    QImage _image(TILESET_WIDTH, TILESET_HEIGHT, QImage::Format_RGB32);

    if(!tileset.isEmpty())
    {        
        _image.fill(Qt::magenta);
        int posx = 0;
        int posy = 0;
        foreach(Tile tile, tileset)
        {
            int i = 0;

            for(int y = 0; y < 8; y++)
            {
                for(int x = 0; x < 8; x++)
                {
                    _image.setPixelColor(x + posx, y + posy, tile.colors[i]);
                    i++;
                }
            }
            posx += 8;
            if(posx == TILESET_WIDTH)
            {
                posx = 0;
                posy += 8;
            }
        }
    }

    return _image;

}

void MainWindow::extractTileSet(void)
{
    if(image.isNull())
    return;

    int width = image.width() / 8;
    int height = image.height() / 8;

    int index = 0;
    for(int y = 0; y < height; y++)
    {
        for(int x = 0; x < width; x++)
        {
            Tile tile = getTile(&image, x * 8, y * 8);

            if(!compareTile(&tileset, &tile))
                tileset.append(tile);
        }

        index++;
    }
}

QImage MainWindow::tileToImage(Tile *tile)
{
    QImage _image(8, 8, QImage::Format_RGB32);

    for(int y = 0; y < 8; y++)
    {
        for(int x = 0; x < 8; x++)
        {
            QColor color = image.pixelColor(tile->posx + x, tile->posy + y);

            _image.setPixelColor(x, y, color);
        }
    }

    return _image;

}

void MainWindow::saveTile(Tile *tile, const QString path, const QString filename)
{
    QImage _image = tileToImage(tile);
    QString _filename = ("{path}/{filemane}.png");

    _filename.replace("{path}", path);
    _filename.replace("{filename}", filename);

    _image.save(_filename);
}

Tile MainWindow::getTile(QImage *_image, int posx, int posy)
{
    Tile tile;

    tile.posx = posx;
    tile.posy = posy;

    int i = 0;
    for(int y = 0; y < 8; y++)
    {
        for(int x = 0; x < 8; x++)
        {
            QColor color = _image->pixelColor(posx + x, posy + y);
            tile.colors[i] = color;
            i++;
        }
    }

    return tile;
}


bool MainWindow::compareTileByPixel(Tile *one, Tile *two)
{
    int match = 0;
    int index = 0;

    for(int y = 0; y < 8; y++)
    {
        for(int x = 0; x < 8; x++)
        {
            if(one->colors[index].toRgb() == two->colors[index].toRgb())
                match++;

            index++;
        }
    }

    return (match == 64);
}

bool MainWindow::compareTile(QList<Tile> *tiles, Tile *tile)
{
    for(int i = 0; i < tiles->size(); i++) {

        Tile _tile = tiles->at(i);

        if(&_tile != tile)
        {
            bool match = match = compareTileByPixel(tile, &_tile);

            if(!match)
            {
                bool a = false, b = false, c = false;
                QImage _image = tileToImage(tile);
                QImage imageFlip;
                Tile tileFlip;

                imageFlip = _image.mirrored(false, true);
                tileFlip = getTile(&imageFlip, 0, 0);
                a = compareTileByPixel(&tileFlip, &_tile);

                if(!a)
                {
                    imageFlip = _image.mirrored(true, true);
                    tileFlip = getTile(&imageFlip, 0, 0);
                    a = compareTileByPixel(&tileFlip, &_tile);
                }

                if(!b)
                {
                    imageFlip = _image.mirrored(true, false);
                    tileFlip = getTile(&imageFlip, 0, 0);
                    c = compareTileByPixel(&tileFlip, &_tile);;
                }

                match = (a || b || c);
            }

            if(match)
                return true;
        }
    }

    return false;
}
