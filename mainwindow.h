#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QPushButton>
#include <QVBoxLayout>
#include <QColor>
#include <QImage>
#include <QFrame>
#include <QList>
#include <QDebug>
#include <QStandardPaths>
#include <QLabel>

typedef struct _Tile
{
    int posx;
    int posy;
    QColor colors[64];
} Tile;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    QImage image;
    QImage imageTileset;
    QPushButton button1;
    QPushButton button2;
    QPushButton button3;
    QPushButton button4;
    QList<Tile> tileset;
    Tile getTile(QImage *_image, int posx, int posy);
    Tile imageToTile(QImage *image);
    void extractTileSet(void);
    bool compareTile(QList<Tile> *tiles, Tile *tile);
    bool compareTileByPixel(Tile *one, Tile *two);
    void saveTile(Tile *tile, const QString path = QDir::homePath(), const QString filename = "tile");
    QImage tileToImage(Tile *tile);
    QImage createTileSet(void);

public slots:
    void load(void);
    void save(void);
    void preview(void);
    void about();

};

#endif // MAINWINDOW_H
